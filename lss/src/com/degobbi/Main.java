package com.degobbi;

import java.util.Arrays;

public class Main {
    static void printaMatr(int[][] m){
        for (int i=0;i<m.length;i++){
            System.out.println(Arrays.toString(m[i]));
        }
    }
    public static String lss(String sLunga, String sCorta){
        int maxLunghezzaCorrente = 0;
        String maxSubCorrente="";
        for(int k=0;k< sLunga.length()-sCorta.length()+1;k++) {
            String sLungaCut = sLunga.substring(k,k+sCorta.length());
            int[][] matr = new int[sLungaCut.length() + 1][sCorta.length() + 1];
            for (int i = 1; i < matr.length; i++) {
                for (int j = 1; j < matr[0].length; j++) {
                    matr[i][j] = (sLungaCut.charAt(i - 1) == sCorta.charAt(j - 1)) ? matr[i - 1][j - 1] + 1 : Math.max(matr[i - 1][j], matr[i][j - 1]);
                    if (matr[i][j] > maxLunghezzaCorrente) {
                        maxLunghezzaCorrente = matr[i][j];
                        maxSubCorrente = sLungaCut.substring(i - maxLunghezzaCorrente, i);

                    }
                }
            }
        }
        System.out.println(maxSubCorrente);
        return maxSubCorrente;

    }

    public static void main(String[] args) {
        lss("oocatocatt","catt");
    }
}
